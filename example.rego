package example

default allow = false

s:=["@", "?", "-", "#", "*",  "%",  "|",  ")",  "(",  "'",  ",", ";" ]
allow =  {
    contains(input.ProductName, s[_]),
    contains(input.ProductDescription, s[_]),
    contains(input.ProductPrice, s[_])
}
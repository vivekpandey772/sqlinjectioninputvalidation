import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductsService } from '../products.service';

@Component({
  selector: 'app-product-edit',
  templateUrl: './product-edit.component.html',
  styleUrls: ['./product-edit.component.css']
})
export class ProductEditComponent implements OnInit {

  angForm: FormGroup;
  product: any = {};
  isNotValidOPAQuery:boolean;

  constructor(private route: ActivatedRoute, private router: Router, private ps: ProductsService, private fb: FormBuilder) {
      this.createForm();
 }

  createForm() {
    this.angForm = this.fb.group({
      ProductName: ['', Validators.required ],
      ProductDescription: ['', Validators.required ],
      ProductPrice: ['', Validators.required ]
    });
    this.isNotValidOPAQuery=false;
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
        this.ps.editProduct(params.id).subscribe(res => {
          this.product = res;
      });
    });
  }

  updateProduct(ProductName, ProductDescription, ProductPrice, id) {
    console.log(ProductName);
    this.route.params.subscribe(params => {

      this.ps.queryOpa(ProductName, ProductDescription, ProductPrice).subscribe(res =>{     
      
        if(Object.values(res).length==1 && Object.values(res)[0].length==1 && Object.values(res)[0][0]==false ){      
          this.ps.updateProduct(ProductName, ProductDescription, ProductPrice, params.id);
          this.router.navigate(['products']);
          this.isNotValidOPAQuery=false;  
        }
        else{
  
          this.isNotValidOPAQuery=true;
        }
    });
      
    });
  }
}

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductsService } from '../products.service';

@Component({
  selector: 'app-product-add',
  templateUrl: './product-add.component.html',
  styleUrls: ['./product-add.component.css']
})
export class ProductAddComponent implements OnInit {

  angForm: FormGroup;
  isNotValidOPAQuery:boolean;
  //result:object;
  constructor(private fb: FormBuilder,private router: Router, private ps: ProductsService) {
    this.createForm();
  }

  createForm() {
    this.angForm = this.fb.group({
      ProductName: ['', Validators.required ],
      ProductDescription: ['', Validators.required ],
      ProductPrice: ['', Validators.required ]
    });
    this.isNotValidOPAQuery=false;
  }

  addProduct(ProductName, ProductDescription, ProductPrice) {    
    this.ps.queryOpa(ProductName, ProductDescription, ProductPrice).subscribe(res =>{     
      
      if(Object.values(res).length==1 && Object.values(res)[0].length==1 && Object.values(res)[0][0]==false ){      
        this.ps.addProduct(ProductName, ProductDescription, ProductPrice);
        this.router.navigate(['products']); 
        this.isNotValidOPAQuery=false;  
      }
      else{

        this.isNotValidOPAQuery=true;
      }
  });    
  
}

  ngOnInit() {
  }

}
